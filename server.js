const express = require("express");
const cors = require("cors");

const port = 3000;

let vClients = [];

const app = new express();
app.use(cors());

app.get("/:name", (request, response, next) => {
  response.send(`Salut`);
});

app.get("/events", (request, response, next) => {

  response.set({
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
  });
  response.write("retry: 10000\n\n");
  
  const vIndex = vClients.findIndex((elt) => elt.ip == request.socket.remoteAddress);
  if (vIndex == -1)
  {    
    console.log(`Connection open : ${request.socket.remoteAddress}`);
    vClients.push({ ip: request.socket.remoteAddress, res: response, nb: 1 });
  }
  else{
    vClients[vIndex].nb++;
    console.log(`${request.socket.remoteAddress} -- Nb incrémenté : ${vClients[vIndex].nb}`);

  }


  request.on("close", () => {
    const vIndex = vClients.findIndex((elt) => elt.ip == request.socket.remoteAddress);
    vClients[vIndex].nb-- ;
    console.log(`Connection closed : ${request.socket.remoteAddress} - nb : ${vClients[vIndex].nb}`);
    if(vClients[vIndex].nb == 0)
      vClients = vClients.filter((elt) => elt.ip != request.socket.remoteAddress);

  });
});

app.get("/send-notification", (request, response, next) => {
  console.log("send-notification", vClients.length);
  vClients.forEach((client) => {
    client.res.write(`data: Mise à jour de bob dans 10 minutes\n\n`);
  });
  response.write(vClients.length.toString());
  response.end();
});

app.listen(port, '0.0.0.0',() => console.log(`je t’écoute depuis le port 3000`));
